import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import AppRouter from './AppRouter';

function App() {
  return (
    <React.Fragment>
      <div className="App">
        <Provider store={store}>
          <AppRouter />
        </Provider>
      </div>
    </React.Fragment>

  );
}

export default App;
