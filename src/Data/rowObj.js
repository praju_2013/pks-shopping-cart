const RowObj = {
    krt:"Kurti",
    jkt:"Jacket",
    jns:"Jeans",
    nlshd:"Nail Shade",
    fcecrm:"Face Cream",
    eyelinr:"Eyeliner",
    lpstck: "Lipstick"
}

export default RowObj;