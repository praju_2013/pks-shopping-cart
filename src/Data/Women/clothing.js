const data = {
    items: [
        {
            name: 'Biba Kurti',
            category: 'clothing',
            code: 'krt_1',
            type: 'krt',
            color: ['Red', 'Pink', 'Brown'],
            size: ['XS', 'S', 'M', 'L'],
            price: 10,
            imgUrl: 'https://n1.sdlcdn.com/imgs/a/m/c/Biba-White-Printed-Cotton-Kurti-SDL663041224-1-de270.jpg'
        },
        {
            name: 'W Kurti',
            category: 'clothing',
            code: 'krt_2',
            type: 'krt',
            color: ['Red', 'Pink', 'Black', 'Green'],
            size: ['XS', 'S', 'M', 'L', 'XL'],
            price: 15,
            imgUrl: 'https://assets.myntassets.com/dpr_2,q_60,w_210,c_limit,fl_progressive/assets/images/6920130/2018/8/10/35c1e939-7e1e-49c2-b418-87a9ab9436801533906585341-W-Women-Green--Charcoal-Yoke-Design-Straight-Kurta-8971533906585161-1.jpg',
        },
        {
            name: 'Aurelia Kurti',
            category: 'clothing',
            code: 'krt_2',
            type: 'krt',
            color: ['Blue', 'Pink', 'Orange'],
            size: ['XS', 'S', 'M', 'L'],
            price: 25,
            imgUrl: 'https://rukminim1.flixcart.com/image/332/398/jj367bk0/kurta/r/h/q/s-17auk13235-62441-aurelia-original-imaf6qqzhtguwpns.jpeg?q=50'

        },
        {
            name: 'Levis Jeans',
            category: 'clothing',
            code: 'jns_1',
            type: 'jns',
            color: ['Blue', 'Pink', 'Orange'],
            size: [28, 30, 32, 34],
            price: 35,
            imgUrl: 'https://media.kohlsimg.com/is/image/kohls/2646700_Underwater_Canyon?wid=500&hei=500&op_sharpen=1'
        },
        {
            name: 'Lee Jeans',
            category: 'clothing',
            code: 'jns_2',
            type: 'jns',
            color: ['Blue', 'Black', 'Orange'],
            size: [28, 30, 32, 34, 36],
            price: 35,
            imgUrl: 'http://assets.myntassets.com/assets/images/7148321/2018/11/6/199e74ca-965f-49db-a524-577c70c99ed91541502247396-Lee-Women-Blue-Skinny-Fit-Low-Rise-Clean-Look-Jeans-15115415-1.jpg'
        },
        {
            name: 'Pepe Jeans',
            category: 'clothing',
            code: 'jns_3',
            type: 'jns',
            color: ['Blue', 'White', 'Brown'],
            size: [28, 30, 32, 34],
            price: 20,
            imgUrl: 'https://www.jeans.ch/out/pictures/generated/product/1/310_412_75/sol_pepe-jeans-women-skinny-blue-saturn-pl201660045_f_1.png'
        },
        {
            name: 'Armani Jeans',
            category: 'clothing',
            code: 'jns_4',
            type: 'jns',
            color: ['Blue', 'White', 'Grey'],
            size: [26, 28, 30, 32, 34],
            price: 10,
            imgUrl: 'http://www.clean-express-69.com/images/pic/42564019QS.jpg'
        },
        {
            name: 'Biba Jackets',
            category: 'clothing',
            code: 'jkt_1',
            type: 'jkt',
            color: ['Red', 'Pink', 'Brown'],
            size: ['XS', 'S', 'M', 'L'],
            price: 24,
            imgUrl: 'https://assets.myntassets.com/h_1440,q_100,w_1080/v1/assets/images/1470770/2016/8/23/11471952246034-BIBA-Red-Ethnic-Jacket-1791471952245952-1.jpg'
        },
        {
            name: 'AKS Jackets',
            category: 'clothing',
            code: 'jkt_2',
            type: 'jkt',
            color: ['Grey', 'Pink', 'Black'],
            size: ['XS', 'S', 'M', 'L', 'XL'],
            price: 15,
            imgUrl: 'https://assets.myntassets.com/dpr_2,q_60,w_210,c_limit,fl_progressive/assets/images/7702665/2018/10/29/3e8dbd8c-048d-4193-b700-bfc7fbb9b1081540816932161-AKS-Women-Jackets-9671540816931991-1.jpg'
        },
        {
            name: 'BitterLime Jackets',
            category: 'clothing',
            code: 'jkt_3',
            type: 'jkt',
            color: ['Red', 'Orange', 'Blue'],
            size: ['XS', 'S', 'M', 'L'],
            price: 10,
            imgUrl: 'https://ak0.scstatic.net/1/cdn2-cont15.sweetcouch.com/149374756902732224-bitterlime-sea-green-block-print-waistcoat.jpg'
        }
      
    ]

};

export default data;