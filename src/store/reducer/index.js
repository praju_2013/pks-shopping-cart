import { combineReducers } from 'redux';
import shopBag from './shopBag';
import refData from './refData'
import order from './order'

const rootReducer = combineReducers ({ refData, shopBag, order });

export default rootReducer;