import { PLACE_ORDER, PLACE_ORDER_SUCCESS, PLACE_ORDER_FAIL } from "../action";

const placeOrderReducer = (state={}, action) => {
    switch(action.type){
        case PLACE_ORDER:
            return {...state, submitting: true};
        case PLACE_ORDER_SUCCESS:
            return {...state, submitting: false, order: action.payload.data};
        case PLACE_ORDER_FAIL:
            return {...state, submitting: false, error: action.payload.error};
        default:
            return state;
        
    }
}

export default placeOrderReducer;