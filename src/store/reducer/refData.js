import {
    FETCH_ITEM_LIST,
    FETCH_ITEM_LIST_SUCCESS,
    FETCH_ITEM_LIST_ERROR
} from "../action";

const refReducer = (refData = {}, action) => {
    switch (action.type) {
        case FETCH_ITEM_LIST:
            return { ...refData, loading: true };
        case FETCH_ITEM_LIST_SUCCESS:
            return { ...refData, loading: false, items: action.payload.data};
        case FETCH_ITEM_LIST_ERROR:
            return { ...refData, loading: false, error: action.payload.error };
        default:
            return refData;
    }
}

export default refReducer;