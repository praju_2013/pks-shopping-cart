import { createStore, applyMiddleware, compose } from 'redux';
import logger from 'redux-logger';
import rootReducer from '../store/reducer'
import thunkMiddleware from 'redux-thunk';


const store = createStore(rootReducer,
    compose(
        applyMiddleware(
            logger,
            thunkMiddleware,
            
        ),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);

export default store;