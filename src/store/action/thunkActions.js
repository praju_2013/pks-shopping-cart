import apiClient from '../../ApiClient';
import { fetchItemList, fetchItemListSuccess, fetchItemListError, placeOrder, placeOrderSuccess, placeOrderFail } from '.';

export const fetchItemsListThunk = (category) => (dispatch, getState) =>{
    dispatch(fetchItemList());
    let url = '/shoppingItems?';
    if(category){
        url= url + 'category=' + category;
    }
    apiClient.get(url)
    .then(({ data }) => {
        dispatch(fetchItemListSuccess(data));
    }).catch((error) => {
        dispatch(fetchItemListError(error.message));
    })

};

export const placeOrderThunk = () => (dispatch, getState) =>{
    dispatch(placeOrder());

    const shopBag = getState().shopBag;
    const request = {
        orderItems: [
            ...shopBag.map((item) => ({
                itemCode: item.code,
                itemColor: item.color,
                itemSize: item.size,
                count: item.count,
            }))
        ]
    };
    
    apiClient.post('/submitOrder', request)
        .then(({ data }) => dispatch(placeOrderSuccess(data)))
        .catch((error) => dispatch(placeOrderFail(error)) );
}

