
export const SET_SHOP_BAG = 'SET_SHOP_BAG';
export const FETCH_ITEM_LIST = 'FETCH_ITEM_LIST';
export const FETCH_ITEM_LIST_SUCCESS = 'FETCH_ITEM_LIST_SUCCESS';
export const FETCH_ITEM_LIST_ERROR = 'FETCH_ITEM_LIST_ERROR';

export const PLACE_ORDER = 'PLACE_ORDER'
export const PLACE_ORDER_SUCCESS = 'PLACE_ORDER_SUCCESS'
export const PLACE_ORDER_FAIL = 'PLACE_ORDER_FAIL'

export const setShoppBag = (updatedData) => ({ type: SET_SHOP_BAG, payload: updatedData });

export const fetchItemList = () => ({ type: FETCH_ITEM_LIST })
export const fetchItemListSuccess = (data) => ({ type: FETCH_ITEM_LIST_SUCCESS, payload: { data } })
export const fetchItemListError = (error) => ({ type: FETCH_ITEM_LIST_ERROR, payload: { error } })

export const placeOrder = () => ({ type: PLACE_ORDER })
export const placeOrderSuccess = (data) => ({ type: PLACE_ORDER_SUCCESS, payload: { data } })
export const placeOrderFail = (error) => ({ type: PLACE_ORDER_FAIL, payload: { error } })
