import axios from 'axios';

const apiClient = axios.create({ baseURL: 'http://192.168.0.4:9081', timeout: 5000 });

export default apiClient;