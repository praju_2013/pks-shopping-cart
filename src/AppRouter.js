import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import HomePage from './component/HomePage';
import ItemList from './component/ItemList';
import Error from './component/Error';
import Specification from './component/ItemList/specifications';
import ShoppingBag from './component/Order/shoppingBag';
import Header from './component/Layout/header';
import OrderConfirmation from './component/Order/OrderConfirmation';

const AppRouter = () => (
    <Router>
        <Route path="/"  component={Header} />
        <Route path="/" exact component={HomePage} />
        <Route path="/items" component={ItemList} />
        <Route path="/specifications" component={Specification} />
        <Route path="/shopping bag" component={ShoppingBag} />
        <Route path="/orderConfirmation" component={OrderConfirmation} />
        <Route path="/error" component={Error} />
    </Router>
);

export default AppRouter;