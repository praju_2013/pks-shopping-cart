import React from 'react';
import Item from './item';


const RowItems = ({ items, title }) => items && items.length > 0 ? (
    <div className="container">
        <div className="row">
            <h3 className="row col-sm-12">{title}...</h3>
            {items.map(el => <Item el={el}/>)}
        </div>
        </div>
) : <div></div>;

export default RowItems;