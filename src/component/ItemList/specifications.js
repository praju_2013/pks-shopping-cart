import React, { useState } from 'react';
import { connect } from 'react-redux';
import { setShoppBag } from '../../store/action';

const Specification = ({ location: { state: { item } }, shopBag, setShopBag, history}) => {
    const [itemColor, setItemColor] = useState();
    const [itemSize, setItemSize] = useState();

    return (
        <div >
            <h3 >Item Details</h3>
            <div className="row ">
                <div className="col-xs-12 col-sm-6 col-md-4 text-center">
                    <div className=" col-sm-12 border border-dark ml-5 mt-2">
                        <img className="col-sm-12 " src={item.imgUrl} style={{ maxWidth: 350, height: 450 }} alt="img" />
                    </div>
                    <span className="col-sm-12" >{item.name}</span>
                </div>
                <div className="col-xs-12 col-sm-6 text-center mt-2 ">
                    <h5 className="col-sm-12" ><b>{item.name}</b></h5>
                    <div>
                    {item.size && item.size.map((el, index) =>
                        (<React.Fragment>
                            <button 
                                className="btn btn-dark mr-2 mt-2"
                                onClick={() => setItemSize(item.size[index])} >
                                {item.size[index]}
                            </button>
                        </React.Fragment>))}
                    </div>
                <div>
                    {item.color && item.color.map((el, index) =>
                        (<React.Fragment >
                            <button 
                                className="btn btn-dark mr-2 mt-2"
                                onClick={() => setItemColor(item.color[index])}>
                                {item.color[index]}
                            </button>
                        </React.Fragment>))}
                    <h5 className="col-sm-12 mt-2">Price:
                    <span className="col-sm-12" >${item.price}</span></h5>
                    <h5 className="col-sm-12 mt-2 text-danger">Item Code:
                        <span className="col-sm-12" >{item.code}</span>
                    </h5>
                </div>
                </div>
            </div>
            <div>
                <button 
                    className={`btn btn-dark mr-2 ${!itemSize ? 'disabled' : ''}`}
                    onClick={() => {
                        const newShopBag = [...shopBag];
                        const itemExist = newShopBag.filter(el => (el.code === item.code) && (el.size === itemSize)).length > 0;
                        if (itemExist) {
                            const cartItem = newShopBag.filter(el => (el.code === item.code) && (el.size === itemSize))[0];
                            cartItem.count = cartItem.count ? cartItem.count + 1 : 2
                        } else {
                            newShopBag.push({ ...item, color: itemColor, size: itemSize, count:'1' });
                        }
                        setShopBag(newShopBag);
                    }}>Add to Bag</button>
                <button className="btn btn-dark" disabled={shopBag.length === 0}
                    onClick={() => history.push('/shopping bag')}>Go to Bag</button>
            </div>

        </div>
    )
};


const mapStoreToProps = (store) => ({
    shopBag: store.shopBag,
});

const mapDispatchToProps = (dispatch) => ({
    setShopBag: updatedData => dispatch(setShoppBag(updatedData))
})
export default connect(mapStoreToProps, mapDispatchToProps)(Specification);

