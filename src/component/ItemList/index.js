import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import RowObj from '../../Data/rowObj';
import RowItems from './rowItems';
import { fetchItemsListThunk } from '../../store/action/thunkActions';

const ItemList = ({ refData, location:{state}, fetchItemList }) => {
    useEffect(() => {
        if(state && state.category){
            fetchItemList(state.category);
        }
    },[state, fetchItemList]);
    return( refData && refData.items ?
    <React.Fragment>
        <h3>Items</h3>
       {Object.keys(RowObj).map(val => 
        <RowItems title={RowObj[val]}
         items={refData.items.filter(el => el.type === val)}/>)}
    </React.Fragment> : 'loading'

)};

const mapStoreToProps = (store) => ({
    refData: store.refData,
});

const mapDispatchToProps = (dispatch) => ({
    fetchItemList: category => dispatch(fetchItemsListThunk(category)),
})

export default connect(mapStoreToProps, mapDispatchToProps)(ItemList);