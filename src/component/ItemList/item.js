import React from 'react';
import { Link } from 'react-router-dom';

const Item = ({ el }) => (
    <div className="col-xs-12 col-sm-6 col-md-4 text-center">
        <div className=" col-sm-12 border border-dark">
            <img className="col-sm-12" src={el.imgUrl} style={{ maxWidth: 200, height: 200 }} alt="img" />
        </div>
        {<Link to={
                 { 
                    pathname: '/specifications',
                    state: {
                        item: el, 
                    }
                }
            }>
        <span className="col-sm-12" >{el.name}</span></Link>}
    </div>
);

export default Item;