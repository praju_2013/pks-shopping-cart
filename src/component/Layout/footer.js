import React from 'react';

const Footer = () => (
    <footer>
        <div className="page-title mt-5 mb-0">
        <div className=" mt-5 mb-0">
                <h4 >Contact Us:
                    <span className="ml-2">pks.au@gmail.com</span>
                </h4>
        </div>
        </div>
    </footer>
);

export default Footer;