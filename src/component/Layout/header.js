import React from 'react';
import DropDown from '../Elements/dropDown';

const Header = () => (
    <div >
        <nav class="navbar navbar-expand-lg navbar-light bg-#900C3F page-title">
            <h3><b>PK's Shopping Cart</b></h3>
            <div className="row ml-auto mr-5">
                <DropDown title="WOMEN" options={[
                    { label: "Clothing", value: "cloth" },
                    { label: "Cosmatics", value: "cos" },
                ]} />
                <DropDown title="MEN" options={[
                    { label: "Clothing", value: "cloth" },
                    { label: "Cosmatics", value: "cos" },
                ]} />
                <DropDown title="KID" options={[
                    { label: "Girls", value: "cloth" },
                    { label: "Boys", value: "cos" },
                ]} />
            </div>
        </nav>
    </div>
);

export default Header;