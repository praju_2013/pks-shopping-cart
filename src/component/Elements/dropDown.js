import React from 'react';
import { Link } from 'react-router-dom';

const DropDown = ({ title, options }) => (
    <div>
        <div className="dropdown ">
            <button className="dropbtn pb-2 pt-2 form-control">{title}</button>
            <div className="dropdown-content">
                {options.map((option, index) => <Link to={{
                    pathname:'/items',
                   state:{
                       category:option.value,
                   }
                }}key={index}>{option.label}</Link>)}
            </div>
        </div>
    </div>

);

export default DropDown;