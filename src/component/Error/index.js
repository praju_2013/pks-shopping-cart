import React from 'react';

const Error = () => (
<div className="container">
    <p className="text-danger">Something went wrong! Please try again later.</p>
</div>);

export default Error;