import React from 'react';
import { connect } from 'react-redux';
import Carousel from 'react-bootstrap/Carousel';
import { fetchItemsListThunk } from '../../store/action/thunkActions';
import Footer from '../Layout/footer';

const HomePage = () => {
return (
    <div className=" container-fluid  home-body ">
        <Carousel>
            <Carousel.Item className="mt-5">
                <img style={{ width: 1000, height: 400 }}
                    src="https://www.easterneye.biz/wp-content/uploads/2018/06/Mit%C3%A4-ostaa-aasiasta-desgintuotteet.jpg"
                    alt="First slide" />
            </Carousel.Item>
            <Carousel.Item className="mt-5">
                <img style={{ width: 1000, height: 400 }}
                    src="https://fashionista.com/.image/t_share/MTQwODY4MjI3NDI4MjYzMjg4/hp-business-casualjpg.jpg"
                    alt="Third slide" />
            </Carousel.Item>
            <Carousel.Item className="mt-5">
                <img style={{ width: 1000, height: 400 }}
                    src="http://cebu.myguide.ph/guide/img/carcar-shoes.jpg"
                    alt="Third slide" />
            </Carousel.Item>
            <Carousel.Item className="mt-5">
                <img style={{ width: 1000, height: 400 }}
                    src="https://cdn.dnaindia.com/sites/default/files/styles/full/public/2019/05/31/830784-cosmetics-store-istock-060119.jpg"
                    alt="Third slide" />
            </Carousel.Item>
            <Carousel.Item className="mt-5">
                <img style={{ width: 1000, height: 400 }}
                    src="https://cdn.shopify.com/s/files/1/0095/0580/0251/files/ViewAllAccessories_Rollover_Shopify_720x300_crop_center.jpg?v=1555950036"
                    alt="Third slide" />
            </Carousel.Item>
        </Carousel>
       
        <Footer />
        
     
    </div>
);
}

const mapStoreToProps = (state) => ({
refData: state.refData,
});

const mapDispatchToProps = (dispatch) => ({
fetchItemsList: () => dispatch(fetchItemsListThunk()),
})

export default connect(mapStoreToProps, mapDispatchToProps)(HomePage);