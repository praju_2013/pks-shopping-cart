import React from 'react';
import { connect } from 'react-redux';

const OrderConfirmation = ({ order }) => (
    <div className="container">
        {order.submitting && <p>...Submitting Order</p>}
        {order.order && <p>Your Order Number is {order.order.orderNumber}</p>}
        {order.error && <p className="text-danger">Error {order.error}</p>}
    </div>
);

const mapStoreToProps = (state) => ({
    order: state.order
})
export default connect(mapStoreToProps)(OrderConfirmation);