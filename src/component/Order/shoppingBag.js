import React from 'react';
import { connect } from 'react-redux';
import { placeOrderThunk } from '../../store/action/thunkActions';

const ShoppingBag = ({ shopBag, setShopBag, submitOrder, history }) => (
    <React.Fragment>
        <h3> Shopping Bag </h3>
        <div className="row border-bottom p-2">
            <div className="col-4"></div>
            <div className="col-2"><strong>Item Details </strong></div>
            <div className="col-2"><strong>Price </strong></div>
            <div className="col-2"><strong>Quantity </strong></div>
        </div>
        {shopBag.map((item, index) => (
            <div key={`shopping-cart-item-${index}`} style={{ alignItems: 'center' }} className="row border-bottom p-2">
                <div className="col-4">
                    <img src={item.imgUrl} style={{ maxWidth: 250, objectFit: "contain" }} alt="shopping-cart-item" />
                </div>
                <div className="col-2" >
                    <span>{item.name}</span><br />
                    {item.color && <span>Color: {item.color}</span>}<br />
                    <span>Size: {item.size}</span>
                </div>
                <div className="col-2" >${item.price}</div>
                <div className="col-2">
                    <select value={item.count ? item.count : 1} onChange={(e) => {
                        const newShopBag = [...shopBag]
                        newShopBag.filter(ele => (item.code === ele.code) && (item.size === ele.size))[0].count = e.target.value;
                        setShopBag(newShopBag)
                    }}>
                        {[...Array(10).keys()].map(number =>
                            <option value={number + 1}>{number + 1}</option>
                        )}
                    </select>
                </div>
                <button className="btn btn-danger " onClick={() => {
                    const newShopBag = [...shopBag];
                    newShopBag.splice(index, 1)
                    setShopBag(newShopBag)
                    }}>Remove
                </button>
            </div>))}
        {(shopBag.length !== 0) ?
            <div className="mt-5">
                <b className="mt-5 mr-auto">
                    Total: ${shopBag.reduce((acc, val) => acc + (val.price * (val.count ? val.count : 1)), 0)}
                </b>
                <button 
                    className="btn btn-dark ml-5" 
                    onClick={() => {
                        submitOrder();
                        history.push('/orderConfirmation')}}>Place Order
                </button>
            </div>
            : null}
    </React.Fragment>
);

const mapStoreToProps = (store) => ({
    shopBag: store.shopBag,
});

const mapDispatchToProps = (dispatch) => ({
    setShopBag: updatedData => dispatch({ type: 'SET_SHOP_BAG', payload: updatedData }),
    submitOrder: () => dispatch(placeOrderThunk()),
})
export default connect(mapStoreToProps, mapDispatchToProps)(ShoppingBag);              